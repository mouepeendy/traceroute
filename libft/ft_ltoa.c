#include "ft_string.h"

static size_t	ft_intlen(int n)
{
	size_t	i;

	i = 1;
	while (n > 9)
	{
		n /= 10;
		i++;
	}
	return (i);
}

static void	ft_fill_array(char *s, int *i, long int n)
{
	if (n < 10)
	{
		s[*i] = n + '0';
		*i += 1;
	}
	else
	{
		ft_fill_array(s, i, n / 10);
		ft_fill_array(s, i, n % 10);
	}
}

char		*ft_ltoa(long long int n)
{
	int	i;
	char	*s;
	int	neg;

	neg = 0;
	i = 0;
	if (n < 0)
	{
		neg = 1;
		n = -n;
	}
	if (!(s = ft_strnew((sizeof(char) * (ft_intlen(n) + neg)))))
		return (NULL);
	if (neg == 1)
		s[0] = '-';
	ft_fill_array(s + neg, &i, n);
	return (s);
}
