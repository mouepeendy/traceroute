#ifndef FT_STDLIB_H
#define FR_STDLIB_H

#include <stddef.h>

int	ft_atoi(const char *str);
char	*ft_itoa(int n);
char	*ft_ltoa(long int n);

#endif
