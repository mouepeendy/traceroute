#ifndef FT_LKLIST_H
#define FT_LKLIST_H

#include <stddef.h>
# include <stdlib.h>

typedef struct	s_list
{
	void		*content;
	size_t		content_size;
	struct s_list	*next;
}t_list;

void 	ft_lstadd(t_list **alst, t_list *new);
void 	ft_lstadd_tail(t_list **alst, t_list *new);
void 	ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void  	ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void  	ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
t_list	*ft_lstnew(void const *content, size_t content_size);

#endif
