#ifndef FT_RANGE_H
#define FT_RANGE_H

#include <stdio.h>
#include <stdlib.h>

int	*ft_range(int min, int max);

#endif
