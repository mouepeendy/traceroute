#ifndef FT_TRACEROUTE_H
#define FT_TRACEROUTE_H

#include <netinet/in.h>

struct traceroute
{
	struct sockaddr_in *sasend;
	struct sockaddr_in *sarecv;
	struct sockaddr_in *salast;
//	struct sockaddr_in *sabind;
	socklen_t salen;
	int /*sport, */dport;
	int max_ttl;
	int max_probe;
	int sendfd, recvfd;
	char g_buff[4096];
	size_t r;
};

static inline void
tvsub(struct timeval *out, struct timeval *in)
{
	if ((out->tv_usec -= in->tv_usec) < 0) {
		--out->tv_sec;
		out->tv_usec += 1000000;
	}
	out->tv_sec -= in->tv_sec;
}

#endif
