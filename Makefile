PROG = ft_traceroute
CC = gcc -g
SRCS = main.c
OBJS = $(SRCS:.c=.o)
LIBFT = libft
LIBFT_LINK = libft/libft.a

all: ${PROG}

main.o: include/ft_traceroute.h libft/include/ft_string.h

.c.o:
	${CC} -Iinclude -Ilibft/include ${CFLAGS} -c $<
	@echo "=> Compiled "$<" successfully!"

${PROG}: ${LIBFT_LINK} ${OBJS}
	${CC} -o $@ ${OBJS} -Llibft -lft
	@echo "=> Linking complete!"

${LIBFT_LINK}:
	make -C ${LIBFT}

clean:
	-rm -f ${OBJS}
	@echo "=> Object files deleted."
	make -C ${LIBFT} clean

fclean:
	-rm -f ${OBJS} ${PROG} ${PROG:=.core}
	@echo "=> All executables deleted."
	make -C ${LIBFT} fclean

re: fclean all

.PHONY: all clean fclean re 
