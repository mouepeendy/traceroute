#include "ft_traceroute.h"
#include "ft_string.h"
#include "ft_stdlib.h"
#include "ft_stdio.h"

#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>

#define DPORT (32768 + 666)
#define BUFSIZE 1500
#define ICMP_HEADER_SIZE 8

int
sock_cmp_addr(const struct sockaddr_in *sa1, const struct sockaddr_in *sa2)
{
	if (sa1->sin_family != sa2->sin_family)
		return (-1);
	return (ft_memcmp(&(sa1->sin_addr),
			&(sa2->sin_addr), sizeof(struct in_addr)));
}

char *
pr_icmpcode(int code)
{
	switch (code) {
	case 0: return(" !N");
	case 1: return(" !H");
	case 2: return(" !P");
	case 3: ;
	case 4: return(" !F");
	case 5: return(" !S");
	case 6: return(" !6");
	case 7: return(" !7");
	case 8: return(" !8");
	case 9: return(" !9");
	case 10: return(" !10");
	case 11: return(" !11");
	case 12: return(" !12");
	case 13: return(" !X");
	case 14: return(" !V");
	case 15: return(" !P");
	}
}

int
recv_tr(struct traceroute *tr, int seq, struct timeval *tv)
{
	int nret, ret;
	int len;
	char recvbuf[BUFSIZE];
	struct ip *ip, *hip;
	struct icmp *icmp;
	struct udphdr *udp;
	int hlen1, hlen2, icmplen;
	struct timeval timeout = {
		.tv_sec = 0,
		.tv_usec = 200000
	};
	fd_set readfds, masterfds;

	for (;;) {
		len = tr->salen;

		FD_ZERO(&masterfds);
		FD_SET(tr->recvfd, &masterfds);
		ft_memcpy(&readfds, &masterfds, sizeof(fd_set));
		if (select(tr->recvfd+1, &readfds, NULL, NULL, &timeout) < 0) {
			exit(EXIT_FAILURE);
		}
		if (FD_ISSET(tr->recvfd, &readfds))
			nret = recvfrom(tr->recvfd, recvbuf, sizeof(recvbuf), 0, (struct sockaddr *)tr->sarecv, &len);
		else
			return (-3);

		if (nret < 0) {
			if (errno == EINTR)
				continue;
			else
				printf("recvfrom error: %s\n", strerror(errno));
		}
		ip = (struct ip *)recvbuf;  /* start of IP header */
		hlen1 = ip->ip_hl << 2; /* length of IP header */

		icmp = (struct icmp *)(recvbuf + hlen1); /* start of ICMP header */
		if ( (icmplen = nret - hlen1) < ICMP_HEADER_SIZE)
			continue; /* less than sizeof(ICMP header) */ 

		if (icmp->icmp_type == ICMP_TIMXCEED &&
				icmp->icmp_code == ICMP_TIMXCEED_INTRANS) {
			if (icmplen < ICMP_HEADER_SIZE + sizeof(struct ip))
				continue; /* no inner IP to read */

			hip = (struct ip *)(recvbuf + hlen1 + ICMP_HEADER_SIZE);
			hlen2 = hip->ip_hl << 2;
			if (icmplen < ICMP_HEADER_SIZE + hlen2 + 4)
				continue; /* not enough data to look at UDP ports */

			udp = (struct udphdr *)(recvbuf + hlen1 + ICMP_HEADER_SIZE + hlen2);
			if (hip->ip_p == IPPROTO_UDP && udp->uh_dport == htons(DPORT + seq)) {
				ret = -2; /* intermediate router */
				break;
			}
		}
		else if (icmp->icmp_type == ICMP_UNREACH) {
			if (icmplen < ICMP_HEADER_SIZE + sizeof(struct ip))
				continue; /* not enough data to look at inner IP */

			hip = (struct ip *)(recvbuf + hlen1 + ICMP_HEADER_SIZE);
			hlen2 = hip->ip_hl << 2;
			if (icmplen < ICMP_HEADER_SIZE + hlen2 + 4)
				continue; /* not enough data to look at UDP ports */

			udp = (struct udphdr *)(recvbuf + hlen1 + ICMP_HEADER_SIZE + hlen2);
			if (hip->ip_p == IPPROTO_UDP && udp->uh_dport == htons(DPORT + seq)) {
				if (icmp->icmp_code == ICMP_UNREACH_PORT)
					ret = -1; /* we reached destination */
				else
					ret = icmp->icmp_code;
				break;
			}
		}
	}
	gettimeofday(tv, NULL);
	return (ret);
}

void
traceloop(struct traceroute *tr)
{
	int seq, ttl, probe;
	int code, done;
	double rtt;
	char *tmp_sec;
	char *tmp_str;
	struct timeval tvstart, tvrecv;

	int block = 0;

	if ( (tr->sendfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		exit(EXIT_FAILURE);
	if ( (tr->recvfd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) < 0)
		exit(EXIT_FAILURE);
	seq = 0;
	done = 0;
	for (ttl = 1; done == 0 && ttl <= tr->max_ttl; ttl++) {
		setsockopt(tr->sendfd, IPPROTO_IP, IP_TTL, &ttl, sizeof(int));
		tmp_str = ft_itoa(ttl);
		int ilen = ft_strlen(tmp_str);
		ft_strcat(tr->g_buff, tmp_str);
		ft_strcat(tr->g_buff, ilen == 2 ? " " :"  ");
		free(tmp_str);
		for (probe = 0; probe < 3; probe++) {
					ft_bzero(tr->sarecv, sizeof(struct sockaddr_in));
			seq++;
			gettimeofday(&tvstart, NULL);
			tr->sasend->sin_port = htons(DPORT + seq);
			sendto(tr->sendfd, "bonjour", 8, 0, (struct sockaddr *)tr->sasend, tr->salen);
			if ( (code = recv_tr(tr, seq, &tvrecv)) == -3) {
				ft_strcat(tr->g_buff, " *");
				block++;
			}
			else {
				if (block)
					block++;
				char str[NI_MAXHOST];

				if (sock_cmp_addr(tr->sarecv, tr->salast) != 0) {
					if (getnameinfo((struct sockaddr *)tr->sarecv,
							tr->salen, str, sizeof(str), NULL, 0, 0) == 0) {
						ft_strcat(tr->g_buff, " ");
						ft_strcat(tr->g_buff, str);
						ft_strcat(tr->g_buff, " (");
						ft_strcat(tr->g_buff, inet_ntoa(tr->sarecv->sin_addr));
						ft_strcat(tr->g_buff, ")");
					}
					else {
						ft_strcat(tr->g_buff, " ");
						ft_strcat(tr->g_buff, inet_ntoa(tr->sarecv->sin_addr));
					}
					ft_memcpy(tr->salast, tr->sarecv, tr->salen);
				}
				tvsub(&tvrecv, &tvstart);
				rtt = tvrecv.tv_sec * 1000.0 + tvrecv.tv_usec / 1000.0;
				ft_strcat(tr->g_buff, " ");
				tmp_sec = ft_itoa((int)rtt);
				ft_strcat(tr->g_buff, tmp_sec);
				ft_strcat(tr->g_buff, ".");
				tmp_str = ft_itoa((rtt - ft_atoi(tmp_sec)) * 1000);
				ft_strcat(tr->g_buff, tmp_str);
				free(tmp_sec);
				free(tmp_str);
				ft_strcat(tr->g_buff, " ms");
				if (code == -1 || code > 0) {
					done++;
					if (code > 0)
						ft_strcat(tr->g_buff, pr_icmpcode(code));
					ft_putstr(tr->g_buff);
					ft_bzero(tr->g_buff, 4096);
					block = 0;
				}
			}
			if (block == 1 || block > 15 || (ttl == 30 && probe == 2)) {
				ft_putstr(tr->g_buff);
				ft_bzero(tr->g_buff, 4096);
				tr->r = 0;
				if (block != 1)
					block = 0;
			}
		}
		if (!block || (ttl == 30 && probe == 2) || done == 1) {
				ft_putstr(tr->g_buff);
				ft_putchar('\n');
				ft_bzero(tr->g_buff, 4096);
				block = 0;
		}
		else if (!(block == 1 && probe == 2)){
			ft_strcat(tr->g_buff, "\n");
		}
	}
}

int
main(int argc, char *argv[])
{
	struct addrinfo hints = {
		.ai_family	= AF_INET,
		.ai_socktype = SOCK_DGRAM,
		.ai_flags = AI_CANONNAME,
	};
	struct traceroute tr;
	char *h;
	struct addrinfo *ai;

	if (getaddrinfo(argv[1], NULL, &hints, &ai) != 0)
		return (-1);
	struct sockaddr_in *sin = (struct sockaddr_in *)ai->ai_addr;
	h = inet_ntoa(sin->sin_addr);
	tr.salen = ai->ai_addrlen;
	tr.sasend = (struct sockaddr_in *)ai->ai_addr;
	if (!(tr.sarecv = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in)))) {
		freeaddrinfo(ai);
	}
	if (!(tr.salast = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in)))) {
		free(tr.sarecv);	
		freeaddrinfo(ai);
	}
	ft_memset(tr.sarecv, 0, sizeof(struct sockaddr_in));
	ft_memset(tr.salast, 0, sizeof(struct sockaddr_in));

	////////////////////////
	tr.max_ttl = 30;
	tr.max_probe = 10;
	tr.r = 0;
	////////////////////////
	ft_bzero(tr.g_buff, 4096);

	traceloop(&tr);
	freeaddrinfo(ai);
	free(tr.sarecv);	
	free(tr.salast);	
	return (0);
}
